'use strict';

// Create express app
const express = require('express');
const app = express();

//Create morgan
const morgan = require('morgan');

//Create path
const path = require('path');

//Create fs
const fs = require('fs');

app.use(express.json());

app.use(morgan('tiny'));

//Create items for my app folder
const folderFiles = [];

fs.readdir(path.join(__dirname, '/api/files'), (error, files) => {
    files.forEach((item) => {
        if (item !== 'empty.txt') {
            folderFiles.push(item);
        }
    });
});

app.get('/api/files', (request, response) => {

    try {
        if (folderFiles.length >= 0) {
            response.status(200).send({message: 'Success', files: folderFiles});
        } else {
            response.status(400).send({message: 'Client error'});
        }
    } catch (e) {
        response.status(500).send({message: 'Server error'});
    }
});

app.post('/api/files', (request, response) => {
    try {

        const {filename, content} = request.body;

        if (filename && content) {

            if (/\.(?:log|txt|json|yaml|xml|js)$/g.test(filename)) {
                fs.writeFile(path.join(path.join(__dirname, '/api/files'), filename), content, () => {
                    console.log('File created successfully');
                });
                response.status(200).send({message: 'File created successfully'});
            }else {
                response.status(400).send({ message: 'Need to use other extensions' });
            }

        } else {
            if (!content) {
                response.status(400).send({message: "Set right content"});
            } else if (!filename) {
                response.status(400).send({ message: "Set right filename" });
            }
        }
    } catch (e) {
        response.status(500).send({message: 'Server error'});
    }
});

app.get('/api/files/:filename', (request, response) => {

    try {
        const filename = request.params.filename;

        if (filename) {
            const result = folderFiles.find((file) => file === filename);
            if (result) {
                const uploadedDate = fs.statSync(path.join(path.join(__dirname, 'api/files'), result)).birthtime;
                const content = fs.readFileSync(path.join(path.join(__dirname, 'api/files'), result)).toString();
                const extension = path.extname(result).split('.')[1];

                response.status(200).send({message: 'Success', filename, content, extension, uploadedDate});
            } else {
                response.status(400).send({message: `No file with ${filename} filename found`});
            }
        }
    } catch (e) {
        response.status(500).send({message: 'Server error'});
    }
});

app.delete('/api/files/:filename', (request, response) => {
    try {
        const filename = request.params.filename;

        if (filename) {
            const result = folderFiles.find((file) => file === filename);

            if (result) {
                fs.unlink(path.join(path.join(__dirname, '/api/files'), result), () => {
                    console.log('File was deleted');
                });
                response.status(200).send({ message: 'File was deleted' });
            } else {
                response.status(400).send({ message: `No file with ${filename} found` });
            }
        }
    } catch (e) {
        response.status(500).send({ message: 'Server error' });
    }
});

app.listen(8080, () => {
    console.log('server are working on the port 8080');
});



